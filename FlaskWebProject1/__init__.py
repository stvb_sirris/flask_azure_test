"""
The flask application package.
"""

from flask import Flask
app = Flask(__name__, debug=True)

import FlaskWebProject1.views
